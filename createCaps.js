const fs = require('fs')
const crypto = require('crypto')
const path = require('path')

const filename = 'caps.json'
const dir = process.argv[3] || __dirname
const netId = process.arg[2] || 'gccNet'

const caps = {
  shs: crypto.randomBytes(32).toString('base64'),
  sign: null,
  invite: crypto.createHash('sha256').update(netId, 'utf-8').digest('base64')
}

fs.writeFileSync(path.join(dir, filename), JSON.stringify(caps, null, 2))
