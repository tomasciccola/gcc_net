# Pruebas con ssb

Acá uso ssb-server para crear una instancia ssb

1. Crear una identidad dentro de una red

Necesito pasarle una configuración. En esa config defino un puerto para escuchar conexiones, una ruta para donde guardar la data (si no existe esa ruta la crea y crea una identidad, si existe carga la identidad que ya está) y un caps. El caps lleva dos campos y uno que es un hash de un string (en este caso gccNet) y otro generado aleatoriamente. Ese caps se debería guardar en un json para compartir con otras personas.
