const Server = require('ssb-server')
  .use(require('ssb-master'))//Sin esto no andan los RPC
const Config = require('ssb-config/inject')

const Client = require('ssb-client')
const keys = require('ssb-keys')

const path = require('path')
const pull = require('pull-stream')
const args = require('minimist')(process.argv.slice(2))

const dir = args.path || path.join(__dirname, 'gccNet')
const cmd = args.cmd

const id = keys.loadOrCreateSync(path.join(dir, 'secret'))
const opts = {
  caps: require('./caps.json'),
  path: dir,
  port: args.port || 8008
}


const config = Config('gccNet', opts)

var server = null

function getSetManifest(){
  var manifest = server.getManifest()
  fs.writeFileSync(
    // Este archivo dice que métodos expone al cliente que se conecte acá
    path.join(config.path, 'manifest.json'),
    JSON.stringify(manifest)
  )
}


switch(cmd){
  case 'start':
    start()
    break
  case 'id':
    getUsrId()
    break
  case 'feed':
    getUsrFeed()
    break
  case 'post':
    post(args.type, args.body)
    break
  case 'follow':
    follow(args.id)
    break
  case 'peers':
    getPeers()
}

function start(){
  server = Server(config)
  server.whoami((err, identidad) => {
    if (err)console.error(err)
    console.log(`holi, tu id --> ${identidad.id}`)
  })
}

function connect(cb){
  Client(id, opts, (err, sbot) => {

    if(sbot === undefined) {
      console.log('el proceso de la red no estaba iniciado')
      console.log('iniciando ahora...')
      console.log('abrite otra ventana de la terminal para poder pedirle cosas')
      start()
      return
    }

    if (err)console.error(err)
    return cb(sbot)
  })
}

function getUsrId(){
  connect(sbot =>{
    sbot.whoami((err, identidad) => {
      if(err)console.error(err)
      console.log(identidad.id)
      sbot.close()
    })
  })
}

function post(type, text){
  if(typeof type !== 'string' || typeof text !== 'string'){
    console.log('posteo malformado :(')
    process.exit(2)
  }
  connect(sbot =>{
    sbot.publish( {type:type, text:text}, (err) =>{
      if(err)console.error(err)
      console.log('ok!')
      sbot.close()
    })
  })
}

function getUsrFeed (){
  connect(sbot => {
    pull(
      sbot.createFeedStream(),
      pull.drain(parseMsgs, function(){
        sbot.close()
      })
    )
  })

  function parseMsgs (m) {
    if(m){
      if(m.value.content) console.log(m.value.content)
    }
  }
}

function follow(id){
  console.log('id', id)
  if(typeof id !== 'string'){
    console.log('mensaje mal formado :(')
    process.exit(2)
  }
  connect(sbot =>{
    sbot.publish({type:'contact', contact:id, following:true}, (err) =>{
      if(err)console.error(err)
      sbot.close()
    })
  })
}

